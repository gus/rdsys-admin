#!/bin/bash

update_repo() {
        git -C ~/repos/$1 stash
        git -C ~/repos/$1 checkout main
        git -C ~/repos/$1 pull
}

build() {
        cd ~/repos/rdsys
        CGO_ENABLED=0 go build -o ~/bin/rdsys-$1 ~/repos/rdsys/cmd/$1
        cd ~
}


echo "WARNING!!! this script is going to remove everything deployed in this user and make a clean deploy."
echo "If you want to continue press ENTER, to cancle press cntrl-c"
read _

update_repo rdsys
update_repo rdsys-admin

rm -rf ~/bin/*
build backend
build distributors
build updaters

rm -rf ~/from-serge/*
cd ~/repos/rdsys
go run scripts/mkdescriptors/main.go ~/from-serge

rm -rf ~/conf/* ~/secrets/*
cp ~/repos/rdsys-admin/conf/* ~/conf/
cp ~/repos/rdsys-admin/test/secrets.json ~/secrets/config.json
touch ~/secrets/blocklist

export XDG_RUNTIME_DIR=/run/user/$(id -u)
rm -rf ~/.config/systemd/user/*
cp ~/repos/rdsys-admin/systemd/* ~/.config/systemd/user/
systemctl --user daemon-reload
systemctl --user enable rdsys-backend
systemctl --user restart rdsys-backend
systemctl --user enable rdsys-moat
systemctl --user restart rdsys-moat

rm -rf ~/logs/* ~/storage/* ~/tmp/*
